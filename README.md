# TypeScript Coding-Dojo

Enthalten sind viele kleine Aufgaben (`/src`) zum erstellen von Typen, welche der Reihe nach erledigt werden sollten.

Es gibt den `no_solutions`-Branch, auf welchem gearbeitet werden sollte. Im `solutions`-Branch sind dann die (Beispiel-)Lösungen zu finden.

Der Master-Branch ist noch ein Überbleibsel und sollte bei Gelegenheit gelöscht werden.

Zur Hilfe liegt noch ein CheatSheet bei (`/src/00-cheat-sheet.md`). Die `notes.md` sind für den Dojo-Leiter als grober Leitfaden, in welcher Reihenfolge die verschiedenen Themen vorgestellt werden sollten. Es empfiehlt sich aber, sich noch eine kleine Präsentation (z.B. als Live Coding) vorzubereiten.

## Aufgaben
Die Aufgaben sind nach Themenblöcken und aufsteigender Schwierigkeit der Themen sortiert. Es empfiehlt sich, sie der Reihe nach zu bearbeiten.

Eine Bearbeitung sieht in der Regel so aus, dass irgendwo ein `type` als `any` definiert wurde. Hier muss das `any` durch einen eigenen Typen ersetze werden, den man ggf. vorher erst anlegen muss.

Sobald die IDE (oder der TypeScript-Compiler) bei einer Datei keine Fehler mehr anzeigt, ist die Aufgabe erfolgreich abgeschlossen und die nächste kann ausgeführt werden.

Es ist nicht notwendig (oder sinnvoll), den Code tatsächlich auszuführen. Wir bewegen uns ausschließlich auf Typ-Ebene.
